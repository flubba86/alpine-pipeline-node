This repo contains the same as [mhart/alpine-node](https://hub.docker.com/r/mhart/alpine-node/), except all images are built with bash installed so that they are able to run in [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines).

Other differences include:

- v0.10.x series is no longer built.
- io.js builds are is moved to another repository (TBD).

vv Below is the original README.md vv

Minimal Node.js Docker Images (18MB, or 6.7MB compressed)
---------------------------------------------------------

Versions v6.2.1, v5.11.1, v4.4.5, v0.12.14, v0.10.45 –
built on [Alpine Linux](https://alpinelinux.org/).

All versions use the one [flubba86/alpine-pipeline-node](https://hub.docker.com/r/flubba86/alpine-pipeline-node/) repository,
but each version aligns with the following tags (ie, `flubba86/alpine-pipeline-node:<tag>`):

- Full install built with npm:
  - `latest`, `6`, `6.2`, `6.2.1` – 46.24 MB (npm 3.9.5)
  - `5`, `5.11`, `5.11.1` – 39.36 MB (npm 3.8.9)
  - `4`, `4.4`, `4.4.5` – 36.31 MB (npm 2.15.6)
  - `0.12`, `0.12.14` – 32.93 MB (npm 2.15.5)
- Base install with node built as a static binary with no npm:
  - `base`, `base-6`, `base-6.2`, `base-6.2.1` – 35.09 MB
  - `base-5`, `base-5.11`, `base-5.11.1` – 27.63 MB
  - `base-4`, `base-4.4`, `base-4.4.5` – 27.23 MB
  - `base-0.12`, `base-0.12.14` – 24.14 MB

Examples
--------

    $ docker run flubba86/alpine-pipeline-node node --version
    v6.2.1

    $ docker run flubba86/alpine-pipeline-node npm --version
    3.9.0

    $ docker run flubba86/alpine-pipeline-node:5 node --version
    v5.11.1

    $ docker run flubba86/alpine-pipeline-node:4 node --version
    v4.4.5

    $ docker run flubba86/alpine-pipeline-node:base node --version
    v6.2.1

Example Dockerfile for your own Node.js project
-----------------------------------------------

If you don't have any native dependencies, ie only depend on pure-JS npm
modules, then my suggestion is to run `npm install` locally *before* running
`docker build` (and make sure `node_modules` isn't in your `.dockerignore`) –
then you don't need an `npm install` step in your Dockerfile and you don't need
`npm` installed in your Docker image – so you can use one of the smaller
`base*` images.

    FROM flubba86/alpine-pipeline-node:base
    # FROM flubba86/alpine-pipeline-node:base-0.12
    # FROM flubba86/alpine-pipeline-node

    WORKDIR /src
    ADD . .

    # If you have native dependencies, you'll need extra tools
    # RUN apk add --no-cache make gcc g++ python

    # If you need npm, don't use a base tag
    # RUN npm install

    EXPOSE 3000
    CMD ["node", "index.js"]

Caveats
-------

All images are now built with the alpine:3.4 base. This is a new release and does not have extensive
testing with node.js 5.x, 4.x or older. If problems arise, we can switch back to building
from the alpine:3.3

As Alpine Linux uses musl, you may run into some issues with environments
expecting glibc-like behaviour (for example, Kubernetes). Some of these issues
are documented here:

- http://gliderlabs.viewdocs.io/docker-alpine/caveats/
- https://github.com/gliderlabs/docker-alpine/issues/8

Inspired by:

- https://github.com/alpinelinux/aports/blob/454db196/main/nodejs/APKBUILD
- https://github.com/alpinelinux/aports/blob/454db196/main/libuv/APKBUILD
- https://hub.docker.com/r/ficusio/nodejs-base/~/dockerfile/
- https://github.com/mhart/alpine-node